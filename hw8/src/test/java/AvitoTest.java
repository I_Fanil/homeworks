import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/java/features",
        glue = "steps",
        plugin = "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm"
)
public class AvitoTest extends AbstractTestNGCucumberTests {
}
