package steps;

public enum Sorting {
    Дефолтно("По умолчанию"),
    Дешевле("Дешевле"),
    Дороже("Дороже"),
    Посвежее("По дате");

    public String id;

    Sorting(String id) {
        this.id = id;
    }
}
