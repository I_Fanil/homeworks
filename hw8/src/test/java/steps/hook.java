package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class hook {
    @Before
    public void before() {
        System.setProperty("webdriver.chrome.driver",
//                "E:\\projects\\school\\chromedriver.exe");
                "C:\\projects\\chromedriver.exe");
        StepsFindPrinter.driver = new ChromeDriver();
        StepsFindPrinter.wait = new WebDriverWait(StepsFindPrinter.driver, 10);
    }

    @After
    public void after() {
        // ожидание перед закрытием браузера
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StepsFindPrinter.driver.quit();
    }
}
