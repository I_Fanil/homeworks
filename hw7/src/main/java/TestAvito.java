import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

public class TestAvito {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver",
//                           "E:\\projects\\school\\chromedriver.exe");
                           "C:\\projects\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        WebElement some;

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);

        // Шаг 1: Открыть ресурс по адресу https://www.avito.ru/
        driver.get("https://www.avito.ru/");

        // Шаг 2: Выбрать “категория” - оргтехника и расходники
        Select categorySelect = new Select(driver
                .findElement(By.cssSelector("#category")));
        categorySelect.selectByVisibleText("Оргтехника и расходники");

        // Шаг 3: В поле поиск по объявлению ввести значение “Принтер”
        // (выдал Exception 1 раз из 10)
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(webDriver ->
                ((JavascriptExecutor) webDriver)
                        .executeScript("return document.readyState")
                        .equals("complete")
        );
        some = driver.findElement(
                By.xpath("//input[@data-marker='search-form/suggest']"));
        some.click();
        some.sendKeys("Принтер");

        // альтернативный вариант Шага 3 - также выдал Exception 1 раз из 10
//        some = wait.until(ExpectedConditions
//        .presenceOfElementLocated(
//                By.xpath("//input[@data-marker='search-form/suggest']")));
//        some.click();
//        some.sendKeys("Принтер");


        // Шаг 4: Нажать на поле город
        driver.findElement(
                By.xpath("//div[@data-marker='search-form/region']")).click();

        // Шаг 5: Заполнить “Владивосток”, кликнуть по первому варианту.
        // Нажать “Показать объявления”
        driver.findElement(By.xpath(
                "//input[@data-marker='popup-location/region/input']"))
                .sendKeys("Владивосток");
        some = driver.findElement(
                By.xpath("//li[@data-marker='suggest(0)']"));
        wait.until(ExpectedConditions
                .textToBePresentInElement(some, "Владивосток"));
        some.click();
        driver.findElement(By.xpath(
                "//button[@data-marker='popup-location/save-button']")).click();

        // Шаг 6: Проверить, активировать чекбокс, нажать “Показать объявления”
        WebElement checkBox = driver.findElement(By.xpath(
                "//span[@data-marker='delivery-filter/text']"));
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);", checkBox);
        if (!checkBox.isSelected()) {
            checkBox.click();
        }
        driver.findElement(By.xpath(
                "//button[@data-marker='search-filters/submit-button']")).click();

        // Шаг 7: В выпадающем списке фильтрации выбрать фильтрацию по убыванию цены.
        Select sortSelect = new Select(driver
                .findElement(By.xpath("//option[text()='Дороже']/..")));
        sortSelect.selectByVisibleText("Дороже");

        // Шаг 8: Вывести в консоль название и стоимость 3х самых дорогих принтеров
        List<WebElement> list = driver
                .findElements(By.xpath(
                        "//div[@data-marker='catalog-serp']/div[@data-item-id]"));
        for (int i=0; i < 3; i++) {
            System.out.println(list.get(i)
                    .findElement(By.cssSelector("[itemprop='name']"))
                    .getText());
            System.out.print(list.get(i)
                    .findElement(By.cssSelector("[itemprop='price']"))
                    .getAttribute("content") + " ");
            System.out.println(list.get(i)
                    .findElement(By.cssSelector("[itemprop='priceCurrency']"))
                    .getAttribute("content") + "\n");
        }

        // ожидание перед закрытием браузера
        try {
            Thread.sleep(2_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}
