package animals;

public class Sheep extends Herbivore implements Voice, Run {
    private int woolLength;               // длина шерсти

    public Sheep(String kind, String name, int weight, double grassPerDay,
                 int woolLength) {
        super(kind, name, weight, grassPerDay);
        this.woolLength = woolLength;
    }

    public int getWoolLength() {
        return woolLength;
    }

    @Override
    public String voice() {
        return "Бее-бее-бее";
    }

    @Override
    public void run() {
        System.out.println(getKind() + " " + getName() + " бежит");
    }
}
