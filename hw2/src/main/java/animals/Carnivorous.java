package animals;

import food.*;

public class Carnivorous extends Animal {
    private double meatPerDay;               // макс. кол-во мяса, съедаемого в день
    private double meatQuantity = 0;         // кол-во мяса, съеденного в день

    public Carnivorous(String kind, String name, int weight, double meatPerDay) {
        super(kind, name, weight);
        this.meatPerDay = meatPerDay;
    }

    public double getMeatPerDay() {
        return meatPerDay;
    }

    public double getMeatQuantity() {
        return meatQuantity;
    }

    @Override
    public void eat(Food food) {
        if (!(food instanceof Meat)) {
            System.out.println("!!! Хищники не едят " + food.getType());
            System.out.println();
            return;
        }
        meatQuantity += food.getPortionOfFood();
        if (meatQuantity > meatPerDay) {
            System.out.println(getKind() + " " + getName() +
                                " уже не ест - он наелся на сегодня!\n");
            return;
        }
        System.out.println(getKind() + " " + getName() + " ест " +
                           food.getType());
        System.out.println();
    }
}