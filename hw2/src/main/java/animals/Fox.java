package animals;

public class Fox extends Carnivorous implements Voice, Run {
    private int woolliness;               // пушистость

    public Fox(String kind, String name, int weight,
               double meatPerDay, int woolliness) {
        super(kind, name, weight, meatPerDay);
        this.woolliness = woolliness;
    }

    public int getWoolliness() {
        return woolliness;
    }

    @Override
    public String voice() {
        return "Тяв-тяв-тяв";
    }

    @Override
    public void run() {
        System.out.println(getKind() + " " + getName() + " бежит");
    }
}
