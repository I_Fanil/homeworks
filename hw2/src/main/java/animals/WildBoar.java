package animals;

public class WildBoar extends Carnivorous implements Voice, Run {
    private int tuskLength;               // длина клыков

    public WildBoar(String kind, String name, int weight,
                double meatPerDay, int tuskLength) {
        super(kind, name, weight, meatPerDay);
        this.tuskLength = tuskLength;
    }

    public int getTuskLength() {
        return tuskLength;
    }

    @Override
    public String voice() {
        return "Хрю-хрю-хрю";
    }

    @Override
    public void run() {
        System.out.println(getKind() + " " + getName() + " бежит");
    }
}
