package animals;

import food.*;

abstract public class Animal {
    private String kind;
    private String name;
    private int weight;

    Animal(String kind, String name, int weight) {
        this.kind = kind;
        this.name = name;
        this.weight = weight;
    }

    public String getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    abstract public void eat(Food food);
}
