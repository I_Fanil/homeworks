package food;

public class Meat extends Food {
    private String partOfCarcass;

    public Meat(String type, double portionOfFood, String partOfCarcass) {
        super(type, portionOfFood);
        this.partOfCarcass = partOfCarcass;
    }

    public String getPartOfCarcass() {
        return partOfCarcass;
    }

    @Override
    public void cultivation() {
        System.out.println("Чтобы " + getType() + " содержала много" +
                "мяса, необходимо давать нужные корма и витамины");
    }
}
