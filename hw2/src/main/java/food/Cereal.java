package food;

public class Cereal extends Grass {
    private int numOfGrains;         // кол-во зерен в колосе

    public Cereal(String type, double portionOfFood, String sort, int numOfGrains) {
        super(type, portionOfFood, sort);
        this.numOfGrains = numOfGrains;
    }

    public int getNumOfGrains() {
        return numOfGrains;
    }
}
