package food;

public abstract class Food {
    private String type;
    private double portionOfFood;               // кол-во еды в порции

    public Food(String type, double portionOfFood) {
        this.type = type;
        this.portionOfFood = portionOfFood;
    }

    public String getType() {
        return type;
    }

    public double getPortionOfFood() {
        return portionOfFood;
    }

    abstract public void cultivation();
}
