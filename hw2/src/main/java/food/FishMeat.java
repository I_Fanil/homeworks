package food;

public class FishMeat extends Meat {
    private String colorOfMeat;                // цвет рыбьего мяса

    public FishMeat(String type, double portionOfFood,
                    String partOfCarcass, String colorOfMeat) {
        super(type, portionOfFood, partOfCarcass);
        this.colorOfMeat = colorOfMeat;
    }

    public String getColorOfMeat() {
        return colorOfMeat;
    }
}
