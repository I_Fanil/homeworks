import animals.*;
import food.*;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker();

        Duck duck = new Duck("Утка", "Понка", 5, 0.6, 80);
        Fish fish1 = new Fish("Рыбка", "Немо", 2, 0.4, 6);
        Fish fish2 = new Fish("Рыбка", "Флаундер", 1, 0.3, 4);
        Sheep sheep = new Sheep("Овца", "Сьюзи", 20, 1.2, 20);

        Wolf wolf = new Wolf("Волк", "Серик", 15, 1.8, 9);
        Fox fox = new Fox("Лис", "Фокси", 10, 1.5, 10);
        WildBoar boar = new WildBoar("Кабан", "Пумба", 40, 2, 7);

        Food[] foods = {
                new Cereal("Злаки", 0.3, "Пшеница", 20),
                new Nut("Орехи", 0.2, "Арахис", 1),
                new Bean("Бобы", 0.3, "Фасоль", 2),
                new RabbitMeat("Крольчатина", 0.8, "Бедро", 5),
                new ChickenMeat("Курятина", 1.2, "Крылышки", 10),
                new FishMeat("Рыбье мясо", 1, "Хвост", "Белый")
        };

        // пруд с плавающими животными
        Swim[] pond = {duck, fish1, fish2};
        System.out.println("Это пруд. Кто же здесь плавает:");
        for (Swim p : pond) {
            p.swim();
        }
        System.out.println();

        // массив с говорящими животными
        Voice[] speakers = {duck, sheep, wolf, fox, boar};
        for (Voice sp : speakers) {
            worker.getVoice(sp);
        }

        // массив со всеми животными
        Animal[] animals = {duck, fish1, fish2, sheep, wolf, fox, boar};

        // этот цикл для того, чтобы несколько раз покормить животных
        // и посмотреть, что они могут наесться
        for (int i = 0; i < 2; i++) {
            for (Animal an : animals) {
                worker.feed(an, (foods[(int) (Math.random() * foods.length)]));
            }
        }
    }
}
