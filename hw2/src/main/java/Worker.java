import animals.*;
import food.*;

public class Worker {
    public void feed(Animal animal, Food food) {
        System.out.println("Работник покормит животное (" +
                            animal.getKind() + " " + animal.getName() +
                            ") едой ("+ food.getType() + ")");
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println("Работник просит животное подать голос");
        System.out.println(((Animal) animal).getKind() + " " +
                            ((Animal) animal).getName() +
                            " говорит: " + animal.voice());
        System.out.println();
    }
}
