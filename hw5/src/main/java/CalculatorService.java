import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CalculatorService {
    private double num;                                 // вводимое с клавиатуры число
    private static int stopper = 0;                     // счетчик попыток ввода числа
    private String[] actions = {"+", "-", "*", "/"};    // массив для арифм. операций
    private Scanner input = new Scanner(System.in);


    // выбор действия для калькулятора
    public String chooseAction() {
        String choose = "";
        System.out.println("Выберите действие (+, -, * или /) и " +
                           "нажмите 'Enter':");
        try {
            choose = input.nextLine();
        } catch (IllegalStateException | NoSuchElementException exc) {
            System.out.println(exc);
        }
        for (String ch : actions) {
            if (choose.equals(ch)) {
                return choose;
            }
        }
        System.out.println("Некорректный выбор! Повторите:");
        return "False";
    }

    // введение числа
    public double enterNumber() {
        stopper++;
        int count = 0;
        System.out.println("Введите число и нажмите 'Enter':");
        try {
            num = Double.parseDouble(input.nextLine());
            count++;
        } catch (NullPointerException | NumberFormatException |
                IllegalStateException | NoSuchElementException exc) {
            System.out.println(exc);
        }
        if (count != 1) {
            System.out.println("Некорректный ввод! Осталось " +
                               (4-stopper) + " попыток:");
            if (stopper < 4) {                   // этот блок кода (про попытки ввода) добавлен, чтобы
                enterNumber();                   // рекурсия завершалась при тестировании
            } else {
                stopper = 0;
                throw new NumberFormatException("Исчерпан лимит " +
                                                "попыток ввода");
            }
        }
        stopper = 0;
        return num;
    }

    // сложение
    public void addition(double num1, double num2) {
        System.out.print(num1 + " + " + num2 + " = " + (num1 + num2));
    }

    // вычитание
    public void subtraction(double num1, double num2) {
        System.out.print(num1 + " - " + num2 + " = " + (num1 - num2));
    }

    // умножение
    public void multiplication(double num1, double num2) {
        DecimalFormat df = new DecimalFormat("0.000");      // для вывода трех знаков после запятой и исключения
        df.setRoundingMode(RoundingMode.DOWN);                // округления крайней цифры в дробной части чисел в тестах
        System.out.print(num1 + " * " + num2 + " = ");
        System.out.print(df.format(num1 * num2));   // отмена округления и три знака после запятой при умножении
    }

    //деление
    public void division(double num1, double num2) {

        if (num2 == 0) {
            System.out.print("Деление на ноль!");
            return;
        }
        DecimalFormat df = new DecimalFormat("0.000");      // для вывода трех знаков после запятой и исключения
        df.setRoundingMode(RoundingMode.DOWN);                // округления крайней цифры в дробной части чисел в тестах
        System.out.print(num1 + " / " + num2 + " = ");
        System.out.print(df.format(num1 / num2));     // отмена округления и три знака после запятой при делении
    }

    public void calculate() {
        switch (chooseAction()) {
            case "+":
                addition(enterNumber(), enterNumber());
                break;
            case "-":
                subtraction(enterNumber(), enterNumber());
                break;
            case "*":
                multiplication(enterNumber(), enterNumber());
                break;
            case "/":
                division(enterNumber(), enterNumber());
                break;
            case "False":
                calculate();
        }
    }
}


