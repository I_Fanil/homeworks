import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.testng.Assert.*;

public class CalculatorServiceTest {
//    private final InputStream systIn = System.in;
    private final PrintStream systOut = System.out;
    private ByteArrayOutputStream bos;

    @BeforeMethod
    public void setUp() {
        bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
    }

    @AfterMethod
    public void tearDown() {
        System.setOut(systOut);
    }

    @DataProvider
    public Object[][] dataForPositiveTestChooseAction() {
        return new Object[][] {
                {"+"}, {"-"}, {"*"}, {"/"}
        };
    }

    @DataProvider
    public Object[][] dataForNegativeTestChooseAction() {
        return new Object[][] {
                {"+ "}, {" *"}, {"++"}, {"h"}, {" "}
        };
    }

    @DataProvider
    public Object[][] dataForPositiveTestEnterNumber() {
        return new Object[][] {
                {"-10201.55", -10201.55},
                {"0", 0.0}, {"+10201.055", 10201.055}
        };
    }

    @DataProvider
    public Object[][] dataForNegativeTestEnterNumber() {
        return new Object[][] {
                {" "}, {"abc"}, {"123.1."}
        };
    }

    @DataProvider
    public Object[][] dataForTestAddition() {
        return new Object[][] {
                {1000, 700, "1000.0 + 700.0 = 1700.0"},
                {-500.57, -5500.41, "-500.57 + -5500.41 = -6000.98"},
                {0, 0, "0.0 + 0.0 = 0.0"}
        };
    }

    @DataProvider
    public Object[][] dataForTestSubtraction() {
        return new Object[][] {
                {1000, 700, "1000.0 - 700.0 = 300.0"},
                {-500.57, -5500.41, "-500.57 - -5500.41 = 4999.84"},
                {0, 0, "0.0 - 0.0 = 0.0"}
        };
    }

    @DataProvider
    public Object[][] dataForTestMultiplication() {
        return new Object[][] {
                {1000, 700, "1000.0 * 700.0 = 700000,000"},
                {-500.57, -5500.41, "-500.57 * -5500.41 = 2753340,233"},
                {0, 0, "0.0 * 0.0 = 0,000"}
        };
    }

    @DataProvider
    public Object[][] dataForTestDivision() {
        return new Object[][] {
                {1000, 25, "1000.0 / 25.0 = 40,000"},
                {-500.57, -5500.41, "-500.57 / -5500.41 = 0,091"},
                {0, 4524, "0.0 / 4524.0 = 0,000"},
                {100, 0, "Деление на ноль!"}
        };
    }

    @DataProvider
    public Object[][] dataForPositiveTestCalculate() {
        return new Object[][] {
                {"+", "100.524", "0.7754", "100.524 + 0.7754 = 101.2994"},
                {"*", "100", "5", "100.0 * 5.0 = 500,000"}
        };
    }

    @Test(dataProvider = "dataForPositiveTestChooseAction",
          testName = "Позитивный тест выбора действия")
    public void positiveTestChooseAction(String symbol) {
        System.setIn(new ByteArrayInputStream(symbol.getBytes()));
        CalculatorService calcTest = new CalculatorService();
        assertEquals(calcTest.chooseAction(), symbol);
    }

    @Test(dataProvider = "dataForNegativeTestChooseAction",
          testName = "Негативный тест выбора действия")
    public void negativeTestChooseAction(String symbols) {
        System.setIn(new ByteArrayInputStream(symbols.getBytes()));
        CalculatorService calcTest = new CalculatorService();
        assertNotEquals(calcTest.chooseAction(), symbols);
    }

    @Test(dataProvider = "dataForPositiveTestEnterNumber",
          testName = "Позитивный тест ввода числа")
    public void positiveTestEnterNumber(String str, double num) {
        System.setIn(new ByteArrayInputStream(str.getBytes()));
        CalculatorService calcTest = new CalculatorService();
        assertEquals(calcTest.enterNumber(), num);
    }

    // из-за наличия нескольких попыток ввода числа в методе enterNumber()
    // негативный тест совмещен с тестом на Exception
    @Test(dataProvider = "dataForNegativeTestEnterNumber",
          testName = "Негативный тест ввода числа",
          expectedExceptions = NumberFormatException.class)
    public void negativeTestEnterNumber(String str) {
        String strIn = str + "\n" + str + "\n" + str + "\n" + str;      // для отрабатывания всех 4 попыток ввода
        System.setIn(new ByteArrayInputStream(strIn.getBytes()));
        CalculatorService calcTest = new CalculatorService();
        calcTest.enterNumber();
    }

    @Test(dataProvider = "dataForTestAddition",
          testName = "Тест сложения")
    public void testAddition(double a, double b, String sum) {
        CalculatorService calcTest = new CalculatorService();
        calcTest.addition(a, b);
        assertEquals(bos.toString(), sum);
    }

    @Test(dataProvider = "dataForTestSubtraction",
            testName = "Тест вычитания")
    public void testSubtraction(double a, double b, String sub) {
        CalculatorService calcTest = new CalculatorService();
        calcTest.subtraction(a, b);
        assertEquals(bos.toString(), sub);
    }

    @Test(dataProvider = "dataForTestMultiplication",
            testName = "Тест умножения")
    public void testMultiplication(double a, double b, String mult) {
        CalculatorService calcTest = new CalculatorService();
        calcTest.multiplication(a, b);
        assertEquals(bos.toString(), mult);
    }

    @Test(dataProvider = "dataForTestDivision",
            testName = "Тест деления")
    public void testDivision(double a, double b, String div) {
        CalculatorService calcTest = new CalculatorService();
        calcTest.division(a, b);
        assertEquals(bos.toString(), div);
    }

    @Test(dataProvider = "dataForPositiveTestCalculate",
            testName = "Позитивный тест Общий")
    public void positiveTestCalculate(String symbol, String num1, String num2, String result) {
        String strIn = symbol + "\n" + num1 + "\n" + num2;
        System.setIn(new ByteArrayInputStream(strIn.getBytes()));
        CalculatorService calcTest = new CalculatorService();
        calcTest.calculate();
        assertTrue(bos.toString().contains(result));
    }
}