import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static io.restassured.RestAssured.given;

public class HomeWorkApiTest {

    // данные для теста метода POST
    @DataProvider
    public Object[][] orderData() {
        return new Object[][] {
            {new Order(new Random().nextInt(10), new Random().nextInt(20),
                       new Random().nextInt(999999), "2021-11-13" +
                       "T19:36:46.978+00:00",true, "approved")},
            {new Order(new Random().nextInt(10), new Random().nextInt(20),
                       new Random().nextInt(999999), "2020-01-01" +
                       "T21:36:46.978+00:00",true, "delivered")}
        };
    }

    // создание объекта для метода setUp()
    private Order order;
    public Order getOrder() {
        order = new Order(
                    new Random().nextInt(10),               // petID
                    new Random().nextInt(20),               // quantity
                    new Random().nextInt(999999),           // order ID
                    "2019-10-10T19:36:46.978+00:00",      // shipDate
                    true,                               // complete
                    "delivered");                           // order status
        return order;
    }

    @BeforeClass
    public void setUp() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://213.239.217.15:9090/api/v3")
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
        // создание заказа
        order = getOrder();
        given()
                .body(order)
            .when().post("/store/order");
    }

    @Test(dataProvider = "orderData")
    public void testPostOrder(Order orderPost) {
        given()
                .body(orderPost)
            .when().post("/store/order")
            .then()
                .statusCode(200);

        Order orderFromStore =
            given()
                    .pathParam("orderId", orderPost.getId())
               .when()
                   .get("/store/order/{orderId}")
               .then()
                   .statusCode(200)
                   .extract().body().as(Order.class);
        Assert.assertEquals(orderFromStore, orderPost);
    }


    @Test(priority = 1)
    public void testGetOrder() {
        given()
                .pathParam("orderId", order.getId())
            .when()
                .get("/store/order/{orderId}")
            .then()
                .statusCode(200)
                .log().all();
    }

    @Test(priority = 2)
    public void testDeleteOrder() {
        given()
                .pathParam("orderId", order.getId())
            .when()
                .delete("/store/order/{orderId}")
            .then()
                .statusCode(200);
        given()
                .pathParam("orderId", order.getId())
            .when()
                .get("/store/order/{orderId}")
            .then()
                .statusCode(404)
                .log().all();
    }

    @Test
    public void testGetInventory() {
        HashMap<String, ?> petInv = new HashMap<>(
        given()
                .baseUri("https://petstore.swagger.io/v2")
            .when()
                .get("/store/inventory")
            .then()
                .statusCode(200)
                .log().all()
                .extract().body().as(Map.class));
        Assert.assertTrue(petInv.containsKey("available"), "HashMap не содержит статус \"available\"");
    }
}
