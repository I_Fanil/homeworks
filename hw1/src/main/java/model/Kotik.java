package model;

public class Kotik {
    private static int numKotiks = 0;   // кол-во котов
    private int prettiness;
    private String name;
    private int weight;
    private String meow;
    private int fullness;               // степень сытости

    public Kotik() {
        Kotik.numKotiks++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        Kotik.numKotiks++;
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public static int getNumKotiks() {
        return numKotiks;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public void eat(int numOfFullness) {
        fullness += numOfFullness;
        System.out.println(name + " что-то поел");
    }

    public void eat(int numOfFullness, String food) {
        fullness += numOfFullness;
        System.out.println(name + " поел " + food);
    }

    public void eat() {
        eat(2, "сметану");
    }

    public boolean play() {
        if(fullness <= 0) {
            System.out.print(name + " отказывается играть. Он просит еды. ");
            return false;
        } else {
            System.out.println(name + " поиграл");
            fullness--;
            return true;
        }
    }

    public boolean sleep() {
        if(fullness <= 0) {
            System.out.print(name + " отказывается спать. Он просит еды. ");
            return false;
        } else {
            System.out.println(name + " поспал");
            fullness--;
            return true;
        }
    }

    public boolean chaseMouse() {
        if(fullness <= 0) {
            System.out.print(name + " отказывается гоняться за мышью. " +
                            "Он просит еды. ");
            return false;
        } else {
            System.out.println(name + " гонялся за мышью");
            fullness--;
            return true;
        }
    }

    public boolean lickFur() {
        if(fullness <= 0) {
            System.out.print(name + " отказывается вылизывать шерстку." +
                             "Он просит еды. ");
            return false;
        } else {
            System.out.println(name + " вылизал шерстку");
            fullness--;
            return true;
        }
    }

    public boolean rubsOnLeg() {
        if(fullness <= 0) {
            System.out.print(name + " отказывается терется " +
                            "о ногу хозяина. Он просит еды. ");
            return false;
        } else {
            System.out.println(name + " потерся о ногу хозяина");
            fullness--;
            return true;
        }
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            System.out.print((i+1) + ". ");
            int j = (int) (Math.random() * 5 + 1);
            boolean deistvie = false;

            switch (j) {
                case 1:
                    deistvie = play();
                    break;
                case 2:
                    deistvie = sleep();
                    break;
                case 3:
                    deistvie = chaseMouse();
                    break;
                case 4:
                    deistvie = lickFur();
                    break;
                case 5:
                    deistvie = rubsOnLeg();
                    break;
            }
            if (!deistvie) {
                eat(4, "кашу");
            }
        }
    }
}
