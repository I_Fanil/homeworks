import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kot1 = new Kotik(8, "Барсик", 5, "Муррр");
        Kotik kot2 = new Kotik();

        kot2.setKotik(9, "Мурзик", 4, "Мяууу");

        if (kot1.getMeow().equals(kot2.getMeow())) {
            System.out.println("Котики разговаривают одинаково: "
                    + kot1.getMeow());
        } else {
            System.out.println("Котики разговаривают по-разному\n"
                                + kot1.getName() + ": " + kot1.getMeow() +
                                ". " + kot2.getName() + ": " + kot2.getMeow());
        }
        System.out.println();

        System.out.println("Это " + kot1.getName()
                            + ". Его вес " + kot1.getWeight() + " кг");
        kot1.liveAnotherDay();
        System.out.println();

        System.out.println("Количество котиков: " + Kotik.getNumKotiks());
    }
}
