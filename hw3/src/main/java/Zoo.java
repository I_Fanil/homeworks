import animals.*;
import static animals.AviarySize.*;

import food.*;

public class Zoo {
    public static void main(String[] args) {
        Worker worker = new Worker();

        Duck duck = new Duck("Утка", "Понка", 5, MEDIUM, 0.6, 80);
        Fish fish1 = new Fish("Рыбка", "Немо", 2, SMALL, 0.4, 6);
        Fish fish2 = new Fish("Рыбка", "Флаундер", 1, SMALL, 0.3, 4);
        Sheep sheep = new Sheep("Овца", "Сьюзи", 20, BIG, 1.2, 20);

        Wolf wolf = new Wolf("Волк", "Серик", 15, BIG, 1.8, 9);
        Fox fox = new Fox("Лис", "Фокси", 10, MEDIUM, 1.5, 10);
        WildBoar boar = new WildBoar("Кабан", "Пумба", 40, OVERSIZE, 2, 7);

        Food[] foods = {
                new Cereal("Злаки", 0.3, "Пшеница", 20),
                new Nut("Орехи", 0.2, "Арахис", 1),
                new Bean("Бобы", 0.3, "Фасоль", 2),
                new RabbitMeat("Крольчатина", 0.8, "Бедро", 5),
                new ChickenMeat("Курятина", 1.2, "Крылышки", 10),
                new FishMeat("Рыбье мясо", 1, "Хвост", "Белый")
        };

        // пруд с плавающими животными
        Swim[] pond = {duck, fish1, fish2};
        System.out.println("Это пруд. Кто же здесь плавает:");
        for (Swim p : pond) {
            p.swim();
        }
        System.out.println();

        // массив с говорящими животными
        Voice[] speakers = {duck, sheep, wolf, fox, boar};
        for (Voice sp : speakers) {
            worker.getVoice(sp);
        }

        // массив со всеми животными
        Animal[] animals = {duck, fish1, fish2, sheep, wolf, fox, boar};

        // этот цикл для того, чтобы несколько раз покормить животных
        // и посмотреть, что они могут наесться
        for (int i = 0; i < 2; i++) {
            for (Animal an : animals) {
                worker.feed(an, (foods[(int) (Math.random() * foods.length)]));
            }
        }

        // создание и наполнение вольера травоядными
        Aviary<Herbivore> aviaryHerb = new Aviary<>(MEDIUM);
        aviaryHerb.addToAviary(duck);
        aviaryHerb.addToAviary(fish2);
        aviaryHerb.addToAviary(fish1);
        aviaryHerb.addToAviary(sheep);
//        aviaryHerb.addToAviary(fox);          // попытка добавить хищника к травоядным
        System.out.println(aviaryHerb);         // показать вольер
        System.out.println();

        // создание и наполнение вольера хищниками
        Aviary<Carnivorous> aviaryCarniv = new Aviary<>(BIG);
        aviaryCarniv.addToAviary(wolf);
        aviaryCarniv.addToAviary(fox);
        aviaryCarniv.addToAviary(boar);
        System.out.println(aviaryCarniv);         // показать вольер
        System.out.println();

        // удаление из вольера животных
        aviaryHerb.removeFromAviary(sheep);
        aviaryHerb.removeFromAviary(fish2);
        System.out.println();

        // получение ссылки на животное из вольера по имени
        Animal refSomeAnimal;
        refSomeAnimal = aviaryHerb.getAnimalRef("Понка");
        System.out.println(refSomeAnimal);

        refSomeAnimal = aviaryCarniv.getAnimalRef("Фокси");
        System.out.println(refSomeAnimal);

        refSomeAnimal = aviaryCarniv.getAnimalRef("Пумба");
        System.out.println(refSomeAnimal);
    }
}
