package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public class Herbivore extends Animal {
    private double grassPerDay;            // макс. кол-во "травы", съедаемой в день
    private double grassQuantity = 0;      // кол-во "травы", съеденной в день

    public Herbivore(String kind, String name, int weight,
                     AviarySize animAvSize, double grassPerDay) {
        super(kind, name, weight, animAvSize);
        this.grassPerDay = grassPerDay;
    }

    public double getGrassPerDay() {
        return grassPerDay;
    }

    public double getGrassQuantity() {
        return grassQuantity;
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Grass)) {
            throw new WrongFoodException("Травоядные", food.getType());
        }
        grassQuantity += food.getPortionOfFood();
        if (grassQuantity > grassPerDay) {
            System.out.println(getKind() + " " + getName() +
                    " уже не ест - она наелась на сегодня!\n");
            return;
        }
        System.out.println(getKind() + " " + getName() + " ест " +
                food.getType());
        System.out.println();

    }
}