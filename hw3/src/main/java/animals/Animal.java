package animals;

import food.Food;
import food.WrongFoodException;

abstract public class Animal {
    private String kind;
    private String name;
    private int weight;
    private AviarySize animAvSize;          // необходимый размер вольера для животного

    Animal(String kind, String name, int weight, AviarySize animAvSize) {
        this.kind = kind;
        this.name = name;
        this.weight = weight;
        this.animAvSize = animAvSize;
    }

    public String getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public AviarySize getAnimAvSize() {
        return animAvSize;
    }

    abstract public void eat(Food food) throws WrongFoodException;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Animal another = (Animal) obj;
        return weight == another.weight &&
                (kind == another.kind || (kind != null &&
                 kind.equals(another.kind))) &&
                (name == another.name || (name != null &&
                 name.equals(another.name))) &&
                (animAvSize == another.animAvSize || (animAvSize != null &&
                 animAvSize.equals(another.animAvSize)));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37*result + (kind == null ? 0 : kind.hashCode());
        result = 37*result + (name == null ? 0 : name.hashCode());
        result = 37*result + weight;
        result = 37*result + (animAvSize == null ? 0 : animAvSize.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return kind + " "+ name;
    }
}
