package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

public class Carnivorous extends Animal {
    private double meatPerDay;               // макс. кол-во мяса, съедаемого в день
    private double meatQuantity = 0;         // кол-во мяса, съеденного в день

    public Carnivorous(String kind, String name, int weight,
                       AviarySize animAvSize, double meatPerDay) {
        super(kind, name, weight, animAvSize);
        this.meatPerDay = meatPerDay;
    }

    public double getMeatPerDay() {
        return meatPerDay;
    }

    public double getMeatQuantity() {
        return meatQuantity;
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Meat)) {
            throw new WrongFoodException("Хищники", food.getType());
        }
        meatQuantity += food.getPortionOfFood();
        if (meatQuantity > meatPerDay) {
            System.out.println(getKind() + " " + getName() +
                    " уже не ест - он наелся на сегодня!\n");
            return;
        }
        System.out.println(getKind() + " " + getName() + " ест " +
                food.getType());
        System.out.println();
    }
}