package animals;

public class Duck extends Herbivore implements Voice, Swim, Fly {
    private int wingspan;               // размах крыльев

    public Duck(String kind, String name, int weight,
                AviarySize animAvSize, double grassPerDay, int wingspan) {
        super(kind, name, weight, animAvSize, grassPerDay);
        this.wingspan = wingspan;
    }

    public int getWingspan() {
        return wingspan;
    }

    @Override
    public String voice() {
        return "Кря-кря-кря";
    }

    @Override
    public void swim() {
        System.out.println(getKind() + " " + getName() + " плывет");
    }

    @Override
    public void fly() {
        System.out.println(getKind() + " " + getName() + " летит");
    }
}
