package animals;

public class Fish extends Herbivore implements Swim {
    private int numOfFins;               // количество плавников

    public Fish(String kind, String name, int weight, AviarySize animAvSize,
                double grassPerDay, int numOfFins) {
        super(kind, name, weight, animAvSize, grassPerDay);
        this.numOfFins = numOfFins;
    }

    public int getNumOfFins() {
        return numOfFins;
    }

    @Override
    public void swim() {
        System.out.println(getKind() + " " + getName() + " плывет");
    }
}
