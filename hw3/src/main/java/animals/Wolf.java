package animals;

public class Wolf extends Carnivorous implements Voice, Run {
    private int smellLevel;               // способность чуять запахи

    public Wolf(String kind, String name, int weight, AviarySize animAvSize,
                double meatPerDay, int smellLevel) {
        super(kind, name, weight, animAvSize, meatPerDay);
        this.smellLevel = smellLevel;
    }

    public int getSmellLevel() {
        return smellLevel;
    }

    @Override
    public String voice() {
        return "У-у-у-у-у (воет)";
    }

    @Override
    public void run() {
        System.out.println(getKind() + " " + getName() + " бежит");
    }
}
