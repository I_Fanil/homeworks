import animals.Animal;
import animals.Voice;
import food.Food;
import food.WrongFoodException;

public class Worker {
    public void feed(Animal animal, Food food) {
        System.out.println("Работник покормит животное " + animal +
                            " едой "+ food.getType());
        try {
            animal.eat(food);
        } catch (WrongFoodException e) {
            System.out.println(e);
        }
    }

    public void getVoice(Voice animal) {
        System.out.println("Работник просит животное подать голос");
        System.out.println(((Animal) animal) +
                            " говорит: " + animal.voice());
        System.out.println();
    }
}
