import animals.*;

import java.util.HashSet;

public class Aviary<T extends Animal> {
    private AviarySize aviarySize;

    public Aviary(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    private HashSet<T> aviary = new HashSet<>();

    public void addToAviary(T animal) {
        if (animal.getAnimAvSize().compareTo(aviarySize) > 0) {
            System.out.println("Этот вольер размером " + aviarySize +
                                " маловат для " + animal.getKind());
            return;
        }
        aviary.add(animal);
        System.out.println("В вольер размером " + aviarySize +
                            " добавлено животное " + animal);
    }

    public void removeFromAviary(T ob) {
        System.out.println("Хотим выгнать из вольера животное " + ob);
        if (aviary.remove(ob)) {
            System.out.println("Выгнали из вольера");
        } else {
            System.out.println("Этого животного в вольере и не было");
            System.out.println();
        }
    }

    public T getAnimalRef(String name) {
        System.out.println("Ищем в вольере животное по имени " + name);
        for (T animal : aviary) {
            if (animal.getName().equals(name)) {
                System.out.print("Вот ссылка на это животное: ");
                return animal;
            }
        }
        System.out.print("В вольере нет животного по имени " + name + " - ");
        return null;
    }

    @Override
    public String toString() {
        return "В вольере: " + aviary;
    }
}
