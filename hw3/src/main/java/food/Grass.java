package food;

public class Grass extends Food {
    private String sort;

    public Grass(String type, double portionOfFood, String sort) {
        super(type, portionOfFood);
        this.sort = sort;
    }

    public String getSort() {
        return sort;
    }

    @Override
    public void cultivation() {
        System.out.println("Чтобы " + getType() + " выросли, " +
                            "их необходимо удобрять, поливать");
    }
}
