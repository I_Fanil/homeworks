package food;

public class ChickenMeat extends Meat {
    private int wingSize;                // размер куриных крылышек

    public ChickenMeat(String type, double portionOfFood,
                       String partOfCarcass, int wingSize) {
        super(type, portionOfFood, partOfCarcass);
        this.wingSize = wingSize;
    }

    public int getWingSize() {
        return wingSize;
    }
}
