package food;

public class WrongFoodException extends Exception {
    String animal;
    String food;

    public WrongFoodException(String animal, String food) {
        this.animal = animal;
        this.food = food;
    }

    @Override
    public String toString() {
        return "Ай-ай-ай! " + animal + " не едят " + food + "!!!\n";
    }
}
