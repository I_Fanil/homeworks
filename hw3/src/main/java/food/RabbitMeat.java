package food;

public class RabbitMeat extends Meat {
    private int fatQty;                // жирность

    public RabbitMeat(String type, double portionOfFood,
                      String partOfCarcass, int fatQty) {
        super(type, portionOfFood, partOfCarcass);
        this.fatQty = fatQty;
    }

    public int getFatQty() {
        return fatQty;
    }
}
