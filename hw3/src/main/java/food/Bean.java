package food;

public class Bean extends Grass {
    private int beanLength;         // длина боба

    public Bean(String type, double portionOfFood, String sort,
                int beanLength) {
        super(type, portionOfFood, sort);
        this.beanLength = beanLength;
    }

    public int getBeanLength() {
        return beanLength;
    }
}
