package food;

public class Nut extends Grass {
    private int nutСaliber;         // диаметр ореха

    public Nut(String type, double portionOfFood, String sort,
               int nutСaliber) {
        super(type, portionOfFood, sort);
        this.nutСaliber = nutСaliber;
    }

    public int getNutСaliber() {
        return nutСaliber;
    }
}
