﻿Запросы заданий с сайта http://www.sql-ex.ru/learn_exercises.php
Исламов Ф.А.

--№ 1.

SELECT model, speed, hd
FROM PC
WHERE price < 500

--№ 2.

SELECT DISTINCT maker
FROM Product
WHERE type = 'Printer'

--№3.

SELECT model, ram, screen
FROM Laptop
WHERE price > 1000

--№4.

SELECT *
FROM Printer
WHERE color = 'y'

--№5.

SELECT model, speed, hd
FROM PC
WHERE price < 600 AND cd IN('12x', '24x')

--№6.

SElECT DISTINCT Maker, speed
FROM Product
INNER JOIN Laptop
ON Product.model = Laptop.model
WHERE hd >= 10
ORDER BY maker, speed

--№7.

SELECT model, price
FROM
(SELECT model, price
FROM Laptop
UNION
SELECT model, price
FROM PC
UNION
SELECT model, price
FROM Printer
) s
WHERE s.model IN 
   (SELECT model
   FROM Product
   WHERE maker = 'B'
   )

--№8.

SELECT DISTINCT maker
FROM Product
WHERE  type = 'PC'
  AND maker NOT IN (
     SELECT maker
     FROM Product
     WHERE  type = 'Laptop')

--№9.

SELECT DISTINCT Maker
FROM Product
WHERE model IN (
     SELECT model
     FROM PC
     WHERE speed >= 450)

--№10.

SELECT model, price
FROM Printer
WHERE price = (
     SELECT MAX(price)
     FROM Printer)

--№11.

SELECT AVG(speed)
FROM PC

--№12.

SELECT AVG(speed)
FROM Laptop
WHERE price > 1000

--№13.

SELECT AVG(speed)
FROM PC
WHERE model IN 
   (SELECT model
   FROM Product
   WHERE maker = 'A'
   )

--№14.

SELECT s1.class, name, country
FROM
   (SELECT class, country
   FROM Classes
   WHERE numGuns >= 10
   ) s1
INNER JOIN Ships
ON s1.class = Ships.class

--№15.

SELECT HD
FROM PC
GROUP BY hd
HAVING COUNT(*) >= 2

--№16.

SELECT DISTINCT A.model, B.model, A.speed, A.ram
FROM PC A, PC B
WHERE A.speed = B.speed
  AND A.ram = B.ram
  AND A.model > B.model

--№17.

SELECT DISTINCT type, L.model, speed
FROM Laptop L, Product P
WHERE L.model = P.model
  AND L.speed < ALL 
     (SELECT speed 
     FROM PC
     )

--№18.

SELECT DISTINCT maker, price
FROM
   (SELECT model, price
   FROM Printer
   WHERE color = 'y' 
   AND price = (
      SELECT MIN(price)
      FROM Printer
      WHERE color = 'y')
   ) A
INNER JOIN Product B
ON A.model = B.model

--№19.

SELECT maker, AVG(screen)
FROM Product A
INNER JOIN
Laptop B
ON A.model = B.model
GROUP BY maker

--№20.

SELECT maker, COUNT(model)
FROM Product A
WHERE type = 'PC'
GROUP BY maker
HAVING COUNT(*) > =3

--№21.

SELECT maker, MAX(price)
FROM Product A
INNER JOIN
PC B
ON A.model = B.model
GROUP BY maker