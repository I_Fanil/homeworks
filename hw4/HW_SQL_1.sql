﻿Запросы заданий для базы AVIA.
Исламов Ф.А.

--1.         Вывести список самолетов с кодами 320, 321, 733;

SELECT AIRCRAFT_CODE, MODEL
FROM AIRCRAFTS_DATA
WHERE AIRCRAFT_CODE IN ('320', '321', '733')

--2.         Вывести список самолетов с кодом не на 3;

SELECT AIRCRAFT_CODE, MODEL
FROM AIRCRAFTS_DATA
WHERE AIRCRAFT_CODE NOT LIKE '3%'

--3.         Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;

SELECT *
FROM TICKETS
WHERE PASSENGER_NAME LIKE 'OLGA%'
	AND (UPPER(EMAIL) LIKE '%OLGA%' OR EMAIL IS NULL )

--4.         Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;

SELECT *
FROM AIRCRAFTS_DATA
WHERE "RANGE" IN (5600, 5700)
ORDER BY "RANGE" DESC

--5.         Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;

SELECT AIRPORT_NAME, CITY
FROM AIRPORTS_DATA
WHERE CITY = 'Moscow'
ORDER BY AIRPORT_NAME

--6.         Вывести список всех городов без повторов в зоне «Europe»;

SELECT DISTINCT CITY, TIMEZONE
FROM AIRPORTS_DATA
WHERE TIMEZONE LIKE 'Eur%'

--7.         Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%

SELECT BOOK_REF, TOTAL_AMOUNT*0.9
FROM BOOKINGS
WHERE BOOK_REF LIKE '3A4%'

--8.         Вывести все данные по местам в самолете с кодом 320 и классом «Business» 
--	строками вида «Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд

SELECT CONCAT('Seat data: ', SEAT_NO) Seat_data
FROM SEATS
WHERE AIRCRAFT_CODE = '320' AND FARE_CONDITIONS = 'Business'

--9.         Найти максимальную и минимальную сумму бронирования в 2017 году;

SELECT MAX(TOTAL_AMOUNT), MIN(TOTAL_AMOUNT)
FROM BOOKINGS
WHERE TRUNC(BOOK_DATE_OLD, 'yy') = TO_DATE('01.17', 'mm.yy')

--10.      Найти количество мест во всех самолетах, вывести в разрезе самолетов;

SELECT AIRCRAFT_CODE, COUNT(*)
FROM SEATS
GROUP BY AIRCRAFT_CODE

--11.      Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест;

SELECT AIRCRAFT_CODE, FARE_CONDITIONS, COUNT(*)
FROM SEATS
GROUP BY AIRCRAFT_CODE, FARE_CONDITIONS
ORDER BY AIRCRAFT_CODE, FARE_CONDITIONS

--12.      Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;

SELECT PASSENGER_NAME, PHONE, COUNT(*) Number_of_Tickets
FROM TICKETS
INNER JOIN
TICKET_FLIGHTS
ON TICKETS.TICKET_NO = TICKET_FLIGHTS.TICKET_NO
WHERE UPPER(PASSENGER_NAME) LIKE 'ALEKSANDR STEPANOV'
		AND PHONE LIKE '%11'
GROUP BY PASSENGER_NAME, PHONE

--13.      Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. 
--	Отсортировать по убыванию количества билетов;

SELECT PASSENGER_NAME, COUNT(*) Number_of_Tickets
FROM TICKETS
INNER JOIN
TICKET_FLIGHTS
ON TICKETS.TICKET_NO = TICKET_FLIGHTS.TICKET_NO
WHERE UPPER(PASSENGER_NAME) LIKE 'ALEKSANDR %'
GROUP BY PASSENGER_NAME
HAVING COUNT(*) >= 2000
ORDER BY COUNT(*) DESC

--14.      Вывести дни в сентябре 2017 с количеством рейсов больше 500.

SELECT TRUNC(SCHEDULED_DEPARTURE, 'dd') days, COUNT(*) num_of_flights
FROM FLIGHTS
WHERE TRUNC(SCHEDULED_DEPARTURE, 'mm') = TO_DATE('09.17', 'mm.yy')
GROUP BY TRUNC(SCHEDULED_DEPARTURE, 'dd')
HAVING COUNT(*) > 500
ORDER BY days

--15.      Вывести список городов, в которых несколько аэропортов

SELECT CITY, COUNT(*) num_of_airports
FROM AIRPORTS_DATA
GROUP BY CITY
HAVING COUNT(*) > 1

--16.      Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата

SELECT AIRCRAFT_CODE
	, LISTAGG (SEAT_NO, ', ') WITHIN GROUP (ORDER BY SEAT_NO) SEAT_NO
FROM SEATS
GROUP BY AIRCRAFT_CODE

--17.      Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017

SELECT *
FROM FLIGHTS
WHERE TRUNC(SCHEDULED_DEPARTURE, 'mm') = TO_DATE('09.17', 'mm.yy')
	AND DEPARTURE_AIRPORT IN (SELECT AIRPORT_CODE
							FROM AIRPORTS_DATA
							WHERE CITY = 'Moscow')
ORDER BY SCHEDULED_DEPARTURE

--18.      Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017

SELECT DEPARTURE_AIRPORT, COUNT(*) num_of_flights
FROM FLIGHTS
WHERE TO_CHAR(SCHEDULED_DEPARTURE, 'yy') = '17'
	AND DEPARTURE_AIRPORT IN (SELECT AIRPORT_CODE
							FROM AIRPORTS_DATA
							WHERE CITY = 'Moscow')
GROUP BY DEPARTURE_AIRPORT

--19.      Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017

SELECT DEPARTURE_AIRPORT, TO_CHAR(SCHEDULED_DEPARTURE, 'mm') months, COUNT(*) num_of_flights
FROM FLIGHTS
INNER JOIN AIRPORTS_DATA
ON FLIGHTS.DEPARTURE_AIRPORT = AIRPORTS_DATA.AIRPORT_CODE
WHERE CITY = 'Moscow' AND TO_CHAR(SCHEDULED_DEPARTURE, 'yy') = '17'
GROUP BY DEPARTURE_AIRPORT, TO_CHAR(SCHEDULED_DEPARTURE, 'mm')
ORDER BY DEPARTURE_AIRPORT, TO_CHAR(SCHEDULED_DEPARTURE, 'mm')

--20.      Найти все билеты по бронированию на «3A4B»

SELECT *
FROM TICKETS
WHERE BOOK_REF LIKE '3A4B%'

--21.      Найти все перелеты по бронированию на «3A4B»

SELECT BOOK_REF, FLIGHTS.*
FROM TICKETS
INNER JOIN TICKET_FLIGHTS
ON TICKETS.TICKET_NO = TICKET_FLIGHTS.TICKET_NO
INNER JOIN FLIGHTS
ON TICKET_FLIGHTS.FLIGHT_ID = FLIGHTS.FLIGHT_ID
WHERE BOOK_REF LIKE '3A4B%'